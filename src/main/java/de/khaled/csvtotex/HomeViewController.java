package de.khaled.csvtotex;

import de.khaled.csvtotex.events.ProgressEventListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;

public class HomeViewController implements ProgressEventListener {
    @FXML
    private AnchorPane mainLayout;

    @FXML
    private TextField templatePath;

    @FXML
    private Button templateBtn;

    @FXML
    private TextField csvPath;

    @FXML
    private Button csvBtn;

    @FXML
    private TextField outputPath;

    @FXML
    private Button outputBtn;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Text progressbarLabel;

    private Process process = new Process();

    @FXML
    private void initialize() {
        templateBtn.setDisable(true);
        csvBtn.setDisable(true);
        progressBar.setProgress(0d);
        process.setEvent(this);
    }

    @FXML
    public void OnTemplateButtonClicked(MouseEvent mouseEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Template File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        File selectedFile = fileChooser.showOpenDialog(mainLayout.getScene().getWindow());

        if (selectedFile != null) {
            var path = selectedFile.getPath();
            templatePath.setText(path);
            csvBtn.setDisable(false);
            Thread thread = new Thread() {
                public void run() {
                    process.setTemplatePath(path);
                }
            };

            thread.start();

        }
    }

    public void OnCSVButtonClicked(MouseEvent mouseEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open CSV File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File selectedFile = fileChooser.showOpenDialog(mainLayout.getScene().getWindow());

        if (selectedFile != null) {
            var path = selectedFile.getPath();
            csvPath.setText(path);
            Thread thread = new Thread() {
                public void run() {
                    process.setCsvPath(path);
                }
            };

            thread.start();
        }
    }

    public void OnOutputButtonClicked(MouseEvent mouseEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(mainLayout.getScene().getWindow());

        if (selectedDirectory != null) {
            var path = selectedDirectory.getPath();
            outputPath.setText(path);
            templateBtn.setDisable(false);
            Thread thread = new Thread() {
                public void run() {
                    process.setOutPutPath(path);
                }
            };

            thread.start();
        }
    }

    @Override
    public void updateProgressBar(double percent, int count, int all) {
        progressBar.setProgress(percent);
        progressbarLabel.setText(count + "/" + all);
    }

}

