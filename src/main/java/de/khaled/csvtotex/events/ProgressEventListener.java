package de.khaled.csvtotex.events;

public interface ProgressEventListener {
    void updateProgressBar(double percent, int count, int all);
}
