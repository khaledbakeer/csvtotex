package de.khaled.csvtotex.events;

import java.util.ArrayList;
import java.util.List;

public class Observer {
    private List<ProgressEventListener> listeners = new ArrayList<ProgressEventListener>();

    public void addListener(ProgressEventListener toAdd) {
        listeners.add(toAdd);
    }

    public void updateProgressBar(double percent, int count, int all) {
        for (ProgressEventListener hl : listeners)
            hl.updateProgressBar(percent, count, all);
    }
}
