package de.khaled.csvtotex;

import de.khaled.csvtotex.events.Observer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Process {
    private String KeyWord = "Timo";
    private int KeyWordNumber = KeyWord.length() + 1;
    private int KeyWordWithBraces = KeyWord.length() + 3;
    LinkedHashMap<Integer, Integer> keyValuePairs = new LinkedHashMap<>();

    private Map<Integer, Integer> _indices = new HashMap<>();

    private String templatePath;
    private String templateString;

    private String csvPath;
    private String csvString;

    private String outPutPath;
    private int count;
    private double progressStatus;
    private Observer event;

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
        readTemplate();
        processTemplate();
    }

    private void processTemplate() {
        var position = this.templateString.indexOf(KeyWord);

        while (position != -1) {
            var index = (int) Character.getNumericValue(this.templateString.charAt(position + KeyWordNumber));
            _indices.put(position, index);

            var left = this.templateString.substring(0, position);
            var right = this.templateString.substring(position + KeyWordWithBraces);
            this.templateString = left + right;
            position = this.templateString.indexOf(KeyWord);
        }

        // order
        _indices.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .forEachOrdered(x -> keyValuePairs.put(x.getKey(), x.getValue()));
    }

    private void readTemplate() {
        try {
            var templateString = Files.readString(Path.of(this.templatePath), StandardCharsets.US_ASCII);
            setTemplateString(templateString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setTemplateString(String templateString) {
        this.templateString = templateString;
    }

    public void setCsvPath(String csvPath) {
        this.csvPath = csvPath;
        readCsv();
        processCsv();
    }

    private void processCsv() {
        var lines = Arrays.stream(this.csvString.split("\\r?\\n")).skip(1);
        var arr = lines.toArray();
        this.count = arr.length;
        int n = 0;
        for (var line : arr) {
            var items = ((String) line).split(";");
            StringBuffer newString = new StringBuffer(this.templateString);


            for (Map.Entry<Integer, Integer> pair : this.keyValuePairs.entrySet()) {
                newString = newString.insert(pair.getKey(), items[pair.getValue()]);
            }

            writeOnDisk(newString, items[0] + ".tex");
            n++;
            this.progressStatus = (1d / this.count) * n;
            event.updateProgressBar(this.progressStatus, n, count);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeOnDisk(StringBuffer text, String name) {
        try {
            Files.writeString(Path.of(this.outPutPath, name), text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readCsv() {
        try {
            var templateString = Files.readString(Path.of(this.csvPath), StandardCharsets.US_ASCII);
            setCsvString(templateString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setCsvString(String csvString) {
        this.csvString = csvString;
    }

    public void setOutPutPath(String outPutPath) {
        this.outPutPath = outPutPath;
    }

    public void setEvent(HomeViewController homeViewController) {
        event = new Observer();
        event.addListener(homeViewController);
    }
}
